<?php
/**
 * @file
 * Drush integration for Sanitize module.
 */

/**
 * Implements hook_drush_command().
 */
function sanitize_drush_command() {
  $commands['sanitize'] = array(
    'description' => 'Sanitizes a Drupal instance, making the database available as a private file.',
    'aliases' => array('san'),
    'arguments' => array(
       'name' => 'The identifier to use for this database export. The file extension will be appended.',
       'procedure' => 'The sanitization procedure plugin to use to sanitize the site. Defaults to "SanitizeDefaultProcedure"',
     ),
     'options' => [
       'admin-pass' => 'Sets the default uid:1 admin password used in the export. Defaults to "admin"',
     ],
     'examples' => array(
       'drush san --admin-pass=test' => 'Sanitize the site and create a new database dump named after the site, with the default sanitization procedure. The password for the uid:1 account will be "test"',
       'drush san foo' => 'Sanitize the site and create a new database dump named "foo", with the default sanitization procedure',
       'drush san foo MySanitizeProcedure' => 'Sanitize the site and create a new database dump named "foo", with the sanitization procedure "MySanitizeProcedure"',
     ),
  );
  $commands['sanitize-list'] = array(
    'description' => 'Provides a list of sanitization procedures available for use.',
    'aliases' => array('sanls'),
     'examples' => array(
       'drush sanls' => 'Provides a list of sanitization procedures available for use.',
     ),
  );

  return $commands;
}

/**
 * Implements drush_COMMAND_NAME().
 */
function drush_sanitize_list() {
  $table = array();
  $plugins = sanitize_get_all_procedure_plugins();
  foreach ($plugins as $name => $metadata) {
    $desc = (!empty($metadata['description'])) ? $metadata['description'] : '';
    $table[] = array($name, $desc);
  }

  if (empty($table)) {
    drush_print(dt('No procedures found.'));
  }
  else {
    array_unshift($table, array('Procedure', 'Description')); 
    drush_print(dt('Sanitization Procedures:'));
    drush_print_table($table, TRUE);
  }
}

/**
 * Implements drush_COMMAND_NAME().
 */
function drush_sanitize($name = NULL, $procedure = 'SanitizeDefaultProcedure') {
  if (NULL === $name) {
    $name = strtolower(preg_replace("/[^A-Za-z0-9]/", '', variable_get('site_name', 'website')));
  }
  $tokens = array('@name' => $name, '@procedure' => $procedure);

  $procedure_instance = sanitize_procedure($procedure);
  if (!$procedure_instance) {
    drush_set_error('SANITIZE_PROCEDURE_NOT_FOUND', dt('Unable to sanitize: Procedure @procedure not found', $tokens)); 
    return;
  }

  $sanitize_path = 'private://sanitize/';
  if (!file_prepare_directory($sanitize_path, FILE_MODIFY_PERMISSIONS | FILE_CREATE_DIRECTORY)) {
    drush_set_error('SANITIZE_DIR_SAVE_ERROR', dt('Unable to save sanitized export: Cannot create private directory.', $tokens));
    return;
  }

  drush_log(dt('Performing database sanitization using @procedure', $tokens));
  $procedure_instance->sanitize();

  drush_log(dt('Sanitization procedure complete. Beginning export.'));
  $temp = drupal_realpath('temporary://');
  $tmp_filepath = drupal_tempnam($temp, 'sanitize-');
  drush_log(dt('Creating @filepath as a temp file', array('@filepath' => $tmp_filepath)));
  drush_invoke_process('@self', 'sql-dump', array(), array('--result-file='. $tmp_filepath), TRUE);

  // Filter the temp file through SanitizeReplaceText to replace the admin password hash.
  $tmp_h = fopen($tmp_filepath, 'r');
  drush_log(dt('Preparing export...', $tokens));
  $procedure_instance->prepareExport($tmp_h);
  drush_log(dt('Moving to private files.'));
  $file = file_save_data($tmp_h, $sanitize_path . $name . '.sql');
  $url = file_create_url($file->uri);
  fclose($tmp_h);

  drush_log(dt('Private file available at @realpath (@url)', array('@realpath' => drupal_realpath($file->uri), '@url' => $url)));

  $procedure_instance->postSanitize();
  drush_log(dt('Cleaning up @tmp_path...', array('@tmp_path' => $tmp_filepath)));
  file_unmanaged_delete($tmp_filepath);


  drush_log(dt('Sanitization complete.'), $type = 'completed');
}
